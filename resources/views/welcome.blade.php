<!DOCTYPE html>
<html>
<head>
    <title></title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('style.css') }}">
    <link rel="stylesheet" href="{{ asset('stopwatch/css/style.css') }}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <style>
        html,body {
            padding:0;
            margin:0;
            height:100%;
        }
        @font-face {
            font-family: 'typewriter';
            src: url('{{ asset('1942_report/1942.ttf') }}'); 
        }
        .key-board{
            width: 100%;
            height: 50%;
            /*background-color: red;*/
            position: fixed;
            bottom: 0;
            z-index: 999!important;
        }
        * {
            margin: 0;
            padding: 0;
        }
        body {
            font: 71%/1.5 Verdana, Sans-Serif;
            background: #eee;
        }
        #container {
            margin: 100px auto;
            width: 688px;
        }
        #write {
            margin: 0 0 5px;
            padding: 5px;
            width: 671px;
            height: 200px;
            font: 1em/1.5 Verdana, Sans-Serif;
            background: #fff;
            border: 1px solid #f9f9f9;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
        }
        #keyboard {
            margin: 0;
            padding: 0;
            list-style: none;
        }
        #keyboard li {
            float: left;
            margin: 0 5px 5px 0;
            width: 40px;
            height: 40px;
            line-height: 40px;
            text-align: center;
            background: #fff;
            border: 1px solid #f9f9f9;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
        }
        .capslock, .tab, .left-shift {
            clear: left;
        }
        #keyboard .tab, #keyboard .delete {
            width: 70px;
        }
        #keyboard .capslock {
            width: 80px;
        }
        #keyboard .return {
            width: 77px;
        }
        #keyboard .left-shift {
            width: 95px;
        }
        #keyboard .right-shift {
            width: 109px;
        }
        .lastitem {
            margin-right: 0;
        }
        .uppercase {
            text-transform: uppercase;
        }
        #keyboard .space {
            clear: left;
            width: 681px;
        }
        .on {
            display: none;
        }
        #keyboard li:hover {
            position: relative;
            top: 1px;
            left: 1px;
            border-color: #e5e5e5;
            /*cursor: pointer;*/
        }
        .pijit{
            background-color: #7fc59e!important;
            color: white;
        }
        .kertas{
            font-family: 'typewriter';
            background: url({{ asset('paper.png') }});background-size: cover; min-height: 1000px;padding-top: 200px;
            padding-left: 120px; 
            padding-right: 120px; 
        }
        .text{
            font-family: 'typewriter';
            font-weight: bold;
            font-size: 20px;
            line-height: 32px;
            position: relative;
        }
        #diketik {
            top: 0;
            width: 100%;
            left: 0;
            position: absolute;
            background-color: transparent;
            min-height: 300px;
            margin-top: 22px;
            font-size: 21px;
            font-weight: bold;
            line-height: 52px;
            border: none;
            /*box-shadow: 3px 3px 10px gray;*/
        }
        .kata-active{
            background-color: white;
            padding: 3px;
            border-radius: 5px;
        }
        .timer-container{
            width: 200px;
            height: 100px;
            background-color: white;
            position: fixed;
            top: 10%;
            right: 0;
            border-radius: 10px;
            box-shadow: 3px 3px 20px lightgray;
        }
        .clock-wrapper span{
            font-size: 27px;
            font-weight: bold;
        }
        .merah { /* className for the trail elements */
            position: absolute;
            height: 10px; width: 10px;
            border-radius: 5px;
            background: red;
        }
        .putih { /* className for the trail elements */
            position: absolute;
            height: 10px; width: 10px;
            border-radius: 5px;
            background: white;
        }
    </style>
</head>
<body>
    <div class="timer-container">
        <div id="timer" class="col-12">
            <div class="clock-wrapper" style="padding-top: 25px;">
                <span class="hours">00</span>
                <span class="dots">:</span>
                <span class="minutes">00</span>
                <span class="dots">:</span>
                <span class="seconds">00</span>
            </div>
            <button class="btn btn-block btn-info btn-sm" id="restart-btn" onclick="mulai_lagi_()">Restart</button>
        </div>
    </div>
    <div class="key-board">
        <div id="container">
            <ul id="keyboard">
                <li class="key-13 symbol"><span class="off">`</span><span class="on">~</span></li>
                <li class="key-1 symbol"><span class="off">1</span><span class="on">!</span></li>
                <li class="key-2 symbol"><span class="off">2</span><span class="on">@</span></li>
                <li class="key-3 symbol"><span class="off">3</span><span class="on">#</span></li>
                <li class="key-4 symbol"><span class="off">4</span><span class="on">$</span></li>
                <li class="key-5 symbol"><span class="off">5</span><span class="on">%</span></li>
                <li class="key-6 symbol"><span class="off">6</span><span class="on">^</span></li>
                <li class="key-7 symbol"><span class="off">7</span><span class="on">&amp;</span></li>
                <li class="key-8 symbol"><span class="off">8</span><span class="on">*</span></li>
                <li class="key-9 symbol"><span class="off">9</span><span class="on">(</span></li>
                <li class="key-0 symbol"><span class="off">0</span><span class="on">)</span></li>
                <li class="key-11 symbol"><span class="off">-</span><span class="on">_</span></li>
                <li class="key-12 symbol"><span class="off">=</span><span class="on">+</span></li>
                <li class="key-delete delete lastitem" style="background-color: #d6b5b5">delete</li>
                <li class="key-tab tab">tab</li>
                <li class="key-q letter">q</li>
                <li class="key-w letter">w</li>
                <li class="key-e letter">e</li>
                <li class="key-r letter">r</li>
                <li class="key-t letter">t</li>
                <li class="key-y letter">y</li>
                <li class="key-u letter">u</li>
                <li class="key-i letter">i</li>
                <li class="key-o letter">o</li>
                <li class="key-p letter">p</li>
                <li class="key-buka symbol"><span class="off">[</span><span class="on">{</span></li>
                <li class="key-tutup symbol"><span class="off">]</span><span class="on">}</span></li>
                <li class="key-garis symbol lastitem"><span class="off">\</span><span class="on">|</span></li>
                <li class="key-caps capslock">caps lock</li>
                <li class="key-a letter">a</li>
                <li class="key-s letter">s</li>
                <li class="key-d letter">d</li>
                <li class="key-f letter">f</li>
                <li class="key-g letter">g</li>
                <li class="key-h letter">h</li>
                <li class="key-j letter">j</li>
                <li class="key-k letter">k</li>
                <li class="key-l letter">l</li>
                <li class="key-tikom symbol"><span class="off">;</span><span class="on">:</span></li>
                <li class="key-kutip symbol"><span class="off">'</span><span class="on">&quot;</span></li>
                <li class="key-return return lastitem">return</li>
                <li class="key-shift left-shift">shift</li>
                <li class="key-z letter">z</li>
                <li class="key-x letter">x</li>
                <li class="key-c letter">c</li>
                <li class="key-v letter">v</li>
                <li class="key-b letter">b</li>
                <li class="key-n letter">n</li>
                <li class="key-m letter">m</li>
                <li class="key-koma symbol"><span class="off">,</span><span class="on">&lt;</span></li>
                <li class="key-titik symbol"><span class="off">.</span><span class="on">&gt;</span></li>
                <li class="key-garing symbol"><span class="off">/</span><span class="on">?</span></li>
                <li class="key-shift2 right-shift lastitem">shift</li>
                <li class="key-32 space lastitem">&nbsp;</li>
            </ul>
        </div>
    </div>
    <div class="container">
        <div class="row ">
            <div class="col-sm-10 offset-sm-1 kertas" >
                <center><span style="font-size: 30px;font-weight: 900;text-decoration: underline;">Proklamasi</span></center>
                <div class="text" style="line-height: 50px;">
                    <span class="kata kata-ke-1">Kami</span> <span class="kata kata-ke-2">bangsa</span> <span class="kata kata-ke-3">Indonesia,</span> <span class="kata kata-ke-4">dengan</span> <span class="kata kata-ke-5">ini</span> <span class="kata kata-ke-6">menjatakan</span> <span class="kata kata-ke-7">kemerdekaan</span> <span class="kata kata-ke-8">Indonesia.</span> <span class="kata kata-ke-9">Hal-hal</span> <span class="kata kata-ke-10">jang</span> <span class="kata kata-ke-11">mengenai</span> <span class="kata kata-ke-12">pemindahan</span> <span class="kata kata-ke-13">kekoeasaan</span> <span class="kata kata-ke-14">d.l.l.,</span> <span class="kata kata-ke-15">diselenggarakan</span> <span class="kata kata-ke-16">dengan</span> <span class="kata kata-ke-17">tjara</span> <span class="kata kata-ke-18">seksama</span> <span class="kata kata-ke-19">dan</span> <span class="kata kata-ke-20">dalam</span> <span class="kata kata-ke-21">tempo</span> <span class="kata kata-ke-22">jang</span> <span class="kata kata-ke-23">sesingkat-</span> <span class="kata kata-ke-24">singkatnja.</span> 
                    <textarea name="" autofocus="" id="diketik"></textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="modal" tabindex="-1" role="dialog" id="done-modal" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Hasil ketikan dirikauu!</h5>
                </div>
                <div class="modal-body">
                    <center><span class="text-info" style="font-size: 30px;font-weight: bold"><span id="kpm"></span> KPM</span> <br> <small>(kata per menit)</small></center>
                    <br>
                    <span class="badge badge-info">Ketepatan : <span id="akurasi"></span></span>
                    <span class="badge badge-success">Kata yang Benar : <span id="benar"></span></span>
                    <span class="badge badge-danger">Kata yang Salah : <span id="salah"></span></span>
                    <br>
                    <br>
                    
                </div>
                <div class="modal-footer">
                    <a class="btn btn-sm btn-default" style="border-radius: 50px;background: #e0e0e0" href="https://www.facebook.com/firman.prayoga.92"><i class="fa fa-facebook"></i></a>
                    <a class="btn btn-sm btn-default" style="border-radius: 50px;background: #e0e0e0" href="https://www.instagram.com/firmanpraa_"><i class="fa fa-instagram"></i></a>
                    <a class="btn btn-sm btn-default" style="border-radius: 50px;background: #e0e0e0" href="https://lumos-indo.com/"><i class="fa fa-globe"></i></a>
                    <button type="button" class="btn btn-primary" onclick="mulai_lagi()">Mulai Lagi</button>
                </div>
            </div>
        </div>
    </div>
</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
<script src="{{ asset('stopwatch/js/script.js') }}"></script>
<script src="{{ asset('main.js') }}"></script>
<script></script>
</html>