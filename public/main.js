
var is_mulai=0;
var kata_ke = 1;
var terketik = "";
const measure = $('select#measure')
const ammount = $('input#num')
const timer = $('#timer')
const s = $(timer).find('.seconds')
const m = $(timer).find('.minutes')
const h = $(timer).find('.hours')

var seconds = 0
var minutes = 0
var hours = 0

var interval = null;

var clockType = undefined;
$(document).ready(function(e){
	$(".kata-ke-1").addClass('kata-active');
});
$(document).keypress(function(event){
	if (is_mulai==0) {
		is_mulai = 1;
		startClock();
	}
	var keycode = (event.keyCode ? event.keyCode : event.which);
	if (event.key==',') {
		$('.key-koma').addClass('pijit');
	}else if(event.key=='.'){
		$('.key-titik').addClass('pijit');
	}else if(event.key=='/'){
		$('.key-garing').addClass('pijit');
	}else if(event.key=='-'){
		$('.key-11').addClass('pijit');
	}else if(event.key=='='){
		$('.key-12').addClass('pijit');
	}else if(event.key=='['){
		$('.key-buka').addClass('pijit');
	}else if(event.key==']'){
		$('.key-tutup').addClass('pijit');
	}else{
		$('.key-'+event.key).addClass('pijit');
	}

	if (keycode==32) {
		kata_ke++;
		$(".kata").removeClass('kata-active');
		$('.kata-ke-'+kata_ke).addClass('kata-active');
		$('.key-'+keycode).addClass('pijit');
	}
});
$(document).keyup(function(event){
	var keycode = (event.keyCode ? event.keyCode : event.which);
	if (event.key==',') {
		$('.key-koma').removeClass('pijit');
	}else if(event.key=='.'){
		$('.key-titik').removeClass('pijit');
	}else if(event.key=='/'){
		$('.key-garing').removeClass('pijit');
	}else if(event.key=='-'){
		$('.key-11').removeClass('pijit');
	}else if(event.key=='='){
		$('.key-12').removeClass('pijit');
	}else if(event.key=='['){
		$('.key-buka').removeClass('pijit');
	}else if(event.key==']'){
		$('.key-tutup').removeClass('pijit');
	}else{
		$('.key-'+event.key).removeClass('pijit');
	}
	if (keycode==32) {
		$('.key-'+keycode).removeClass('pijit');
	}
});
$(document).click(function(event){
	$("#diketik").focus();
});
$("#diketik").on('keydown', function (e) {
	if (e.keyCode == 8 ) {
		e.preventDefault();
	}
	terketik = $("#diketik").val();
	if (kata_ke>=24) {
		$("#done-modal").modal('show');
		pauseClock();
		penilaian();
	}
});

function mulai_lagi() {
	$("#done-modal").modal('toggle');
	kata_ke = 1;
	$("#diketik").val('');
	is_mulai = 0;
	terketik = "";
	$(".kata").removeClass('kata-active');
	$(".kata-ke-1").addClass('kata-active');
	restartClock();
}
function mulai_lagi_() {
	kata_ke = 1;
	$("#diketik").val('');
	is_mulai = 0;
	terketik = "";
	$(".kata").removeClass('kata-active');
	$(".kata-ke-1").addClass('kata-active');
	restartClock();
}
function pad(d) {
	return (d < 10) ? '0' + d.toString() : d.toString()
}

function startClock() {
	hasStarted = false
	hasEnded = false

	seconds = 0
	minutes = 0
	hours = 0

	switch ($(measure).val()) {
		case 's':
		if ($(ammount).val() > 3599) {
			let hou = Math.floor($(ammount).val() / 3600)
			hours = hou
			let min = Math.floor(($(ammount).val() - (hou * 3600)) / 60)
			minutes = min;
			let sec = ($(ammount).val() - (hou * 3600)) - (min * 60)
			seconds = sec
		}
		else if ($(ammount).val() > 59) {
			let min = Math.floor($(ammount).val() / 60)
			minutes = min
			let sec = $(ammount).val() - (min * 60)
			seconds = sec
		}
		else {
			seconds = $(ammount).val()
		}
		break
		case 'm':
		if ($(ammount).val() > 59) {
			let hou = Math.floor($(ammount).val() / 60)
			hours = hou
			let min = $(ammount).val() - (hou * 60)
			minutes = min
		}
		else {
			minutes = $(ammount).val()
		}
		break
		case 'h':
		hours = $(ammount).val()
		break
		default:
		break
	}

	if (seconds <= 10 && clockType == 'countdown' && minutes == 0 && hours == 0) {
		$(timer).find('span').addClass('red')
	}

	refreshClock()

	$('.input-wrapper').slideUp(350)
	setTimeout(function(){
		$('#timer').fadeIn(350)
		$('#stop-timer').fadeIn(350)

	}, 350)

	cronometer();
}

function restartClock() {
	clear(interval)
	hasStarted = false
	hasEnded = false

	seconds = 0
	minutes = 0
	hours = 0

	$(s).text('00')
	$(m).text('00')
	$(h).text('00')

	$(timer).find('span').removeClass('red')
}

function pauseClock() {
	clear(interval)
	$('#resume-timer').fadeIn()
	$('#reset-timer').fadeIn()
}

var hasStarted = false
var hasEnded = false
if (hours == 0 && minutes == 0 && seconds == 0 && hasStarted == true) {
	hasEnded = true
}

function countdown() {
	hasStarted = true
	interval = setInterval(() => {
		if(hasEnded == false) {
			if (seconds <= 11 && minutes == 0 && hours == 0) {
				$(timer).find('span').addClass('red')
			}

			if(seconds == 0 && minutes == 0 || (hours > 0  && minutes == 0 && seconds == 0)) {
				hours--
				minutes = 59
				seconds = 60
				refreshClock()
			}

			if(seconds > 0) {
				seconds--
				refreshClock()
			}
			else if (seconds == 0) {
				minutes--
				seconds = 59
				refreshClock()
			}
		}
		else {
			restartClock()
		}

	}, 1000)
}

function cronometer() {
	hasStarted = true
	interval = setInterval(() => {
		if (seconds < 59) {
			seconds++
			refreshClock()
		}
		else if (seconds == 59) {
			minutes++
			seconds = 0
			refreshClock()
		}

		if (minutes == 60) {
			hours++
			minutes = 0
			seconds = 0
			refreshClock()
		}

	}, 1000)
}

function refreshClock() {
	$(s).text(pad(seconds))
	$(m).text(pad(minutes))
	if (hours < 0) {
		$(s).text('00')
		$(m).text('00')
		$(h).text('00')
	} else {
		$(h).text(pad(hours))
	}

	if (hours == 0 && minutes == 0 && seconds == 0 && hasStarted == true) {
		hasEnded = true
		alert('The Timer has Ended !')
	}
}

function clear(intervalID) {
	clearInterval(intervalID)
}

function penilaian() {
	var arr_diketik = terketik.split(' ');
        // console.log(terketik);
        var benar = 0;
        var kpm = Math.round(24/(seconds/60));
        $("#kpm").html(kpm);
        var persentase = 0;
        for (var i = 1 ; i <= 24; i++) {
        	if ($(".kata-ke-"+i).html()==arr_diketik[i-1]) {
        		benar++;
        	}
        }
        persentase = (benar/24) * 100;
        $("#akurasi").html(persentase+"%");
        $("#benar").html(benar);
        $("#salah").html(24-benar);
    }
// dots is an array of Dot objects,
// mouse is an object used to track the X and Y position
   // of the mouse, set with a mousemove event listener below
   var dots = [],
   mouse = {
   	x: 0,
   	y: 0
   };

// The Dot object used to scaffold the dots
var Dot = function() {
	this.x = 0;
	this.y = 0;
	this.node = (function(){
		var n = document.createElement("div");
		if (Math.floor(Math.random() * 10)%2==0) {
			n.className = "merah";
		}else{
			n.className = "putih";
		}
		document.body.appendChild(n);
		return n;
	}());
};
// The Dot.prototype.draw() method sets the position of 
  // the object's <div> node
  Dot.prototype.draw = function() {
  	this.node.style.left = this.x + "px";
  	this.node.style.top = this.y + "px";
  };

// Creates the Dot objects, populates the dots array
for (var i = 0; i < 12; i++) {
	var d = new Dot();
	dots.push(d);
}

// This is the screen redraw function
function draw() {
  // Make sure the mouse position is set everytime
    // draw() is called.
    var x = mouse.x,
    y = mouse.y;

  // This loop is where all the 90s magic happens
  dots.forEach(function(dot, index, dots) {
  	var nextDot = dots[index + 1] || dots[0];

  	dot.x = x;
  	dot.y = y;
  	dot.draw();
  	x += (nextDot.x - dot.x) * .6;
  	y += (nextDot.y - dot.y) * .6;

  });
}

addEventListener("mousemove", function(event) {
  //event.preventDefault();
  mouse.x = event.pageX;
  mouse.y = event.pageY;
});

// animate() calls draw() then recursively calls itself
  // everytime the screen repaints via requestAnimationFrame().
  function animate() {
  	draw();
  	requestAnimationFrame(animate);
  }

// And get it started by calling animate().
animate();
